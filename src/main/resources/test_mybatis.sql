/*
Navicat MySQL Data Transfer

Source Server         : mydata
Source Server Version : 50720
Source Host           : localhost:3306
Source Database       : test_mybatis

Target Server Type    : MYSQL
Target Server Version : 50720
File Encoding         : 65001

Date: 2019-07-10 11:45:36
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `test_issue`
-- ----------------------------
DROP TABLE IF EXISTS `test_issue`;
CREATE TABLE `test_issue` (
  `id` int(10) DEFAULT NULL,
  `name` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of test_issue
-- ----------------------------
INSERT INTO `test_issue` VALUES ('1', 'newName1562729880596');
