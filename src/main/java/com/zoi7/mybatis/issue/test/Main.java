package com.zoi7.mybatis.issue.test;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.Reader;

/**
 * @author yjy
 * 2019-07-09 16:49
 */
public class Main {

    /**
     * Run this to reproduce issue
     * @param args ..
     * @throws IOException
     * @throws InterruptedException
     */
    public static void main(String[] args) throws IOException, InterruptedException {
        // just do it...
        String resource = "mybatis-conf.xml";
        Reader reader = Resources.getResourceAsReader(resource);
        SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(reader);

        Thread find1 = new Thread(() -> {
            SqlSession session = factory.openSession();
            System.out.println(session.selectOne("com.zoi7.mybatis.issue.test.mapper.IssueMapper.findOne").toString());
            session.commit();
            session.close();
        });

        Thread update1 = new Thread(() -> {
            SqlSession session = factory.openSession();
            session.update("com.zoi7.mybatis.issue.test.mapper.IssueMapper.update", "newName" + System.currentTimeMillis());
            session.commit();
            session.close();
        });

        Thread find2 = new Thread(() -> {
            SqlSession session = factory.openSession();
            System.out.println(session.selectOne("com.zoi7.mybatis.issue.test.mapper.IssueMapper.findOne").toString());
            session.commit();
            session.close();
        });

        // first query
        find1.start();
        find1.join();

        // update record
        update1.start();
        update1.join();

        // second query (same sql with thread[find1])
        find2.start();
        find2.join();

    }

}
