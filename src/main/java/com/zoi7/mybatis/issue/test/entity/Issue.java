package com.zoi7.mybatis.issue.test.entity;

import java.io.Serializable;

/**
 * @author yjy
 * 2019-07-09 16:45
 */
public class Issue implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private String name;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Issue{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
