package com.zoi7.mybatis.issue.test.mapper;

import com.zoi7.mybatis.issue.test.entity.Issue;
import org.apache.ibatis.annotations.Param;

/**
 * @author yjy
 * 2019-07-09 16:44
 */
public interface IssueMapper {

    Issue findOne();

    void update(@Param("name") String name);

}
